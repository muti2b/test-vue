// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import Test from './components/Test.vue'
import Hello from './components/Hello.vue'
import CssTest from './components/CssTest.vue'

Vue.use(VueRouter)
Vue.use(VueResource)

const routes = [
  {path:'/test/:testRoute',component:Test},
  {path:'/hello/:hello',component:Hello},
  {path:'*',component:CssTest},
  {path:'css',component:CssTest}
]

const router = new VueRouter({
  routes
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
